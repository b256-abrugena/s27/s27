let users = `[
    {
      "id": "polomints",
      "firstName": "David Polo",
      "lastName": "Abrugena",
      "email": "dpbabrugena@gmail.com",
      "password": "user1234",
      "isAdmin": "false",
      "mobileNumber": "09065968140",
      "orders":{
          "id": "AB987",
          "transactionDate": "02-24-2023",
          "status": "shipping",
          "total": "50"
      }
    },
    {
      "id": "galpos3",
      "firstName": "Haneul",
      "lastName": "Kang",
      "email": "kimsky@gmail.com",
      "password": "user5678",
      "isAdmin": "true",
      "mobileNumber": "09159964014",
      "orders":{
          "id": "EF321",
          "transactionDate": "02-21-2023",
          "status": "delivered",
          "total": "21"
      }
    }
  ]`;

  let products = `[
    {
      "id": "CD654",
      "productName": "Quaker Oats 100g",
      "description": "Instant Oatmeal",
      "price": "Php 150.00",
      "stocks": "31",
      "isActive": "true",
      "sku": "ABC12345",
      "otherProducts": {
          "orderID": "ZY0001",
          "quantity": "27",
          "price": "Php 55.00",
          "subTotal": "29"
      }
    },
    {
      "id": "GH456",
      "productName": "Gardenia Classic 250g",
      "description": "Enriched White Bread",
      "price": "Php 70.00",
      "stocks": "42",
      "isActive": "true",
      "sku": "DEF67890",
      "otherProducts": {
          "orderID": "XW0002",
          "quantity": "31",
          "price": "Php 66.00",
          "subTotal": "52"
      }
    }
  ]`; 

  console.log(JSON.parse(users));
  console.log(JSON.parse(products));